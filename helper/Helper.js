var config = require('../config/config');
var Order = require('../models/orders');
var client;  
exports = module.exports = {
    createOrder: function(id) {
       console.log('--helper - create Order---');
       var temp = {
                street: '123 Fake Street',
                city: 'Faketon',
                state: 'MA',
                zip: '12345'
            };
        // create a new Order
        var newOrder = Order({
            _id: id,
            name: 'thirupathi',
            orderInfo:temp
        });
        // save the Order
        newOrder.save(function(err) {
            if (err) throw err;
            console.log('Order created!');
        }); 
    },
    getAllOrders: function() {
        // get all the Orders
        Order.find({}, function(err, result) {
            if (err) throw err;

            // object of all the Orders
            console.log(result);
        });

    },
    getOrderById: function(id) {
        // get a Order with ID
        Order.findById(id, function(err, result) {
            if (err) throw err;
            // show the one obj
            console.log(result);
        });
    },
    removeOrderById: function(id) {
        // find the Order with ID 
        Order.findByIdAndRemove(id, function(err) {
            if (err) throw err;
            // we have deleted the Order
            console.log('Order deleted!');
        });
    }
}
exports.client = client; 