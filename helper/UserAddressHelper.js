var config = require('../config/config');
var Address = require('../models/userAddresses');
var client;  
exports = module.exports = {
    createAddress: function(id) {
       console.log('--helper - create Address---');
       var temp = {
                street: '123 Fake Street',
                city: 'Faketon',
                state: 'MA',
                zip: '12345'
            };
        // create a new user Address
        var newAddress = Address({
            _id: id,
            name: 'thirupathi',
            address:temp
        });
        // save the user Address
        newAddress.save(function(err) {
            if (err) throw err;
            console.log('Address created!');
        }); 
    },
    getAllAddresses: function() {
        // get all the user Addresses
        Address.find({}, function(err, result) {
            if (err) throw err;

            // object of all the user Addresses
            console.log(result);
        });

    },
    getAddressById: function(id) {
        // get a user Address with ID
        Address.findById(id, function(err, result) {
            if (err) throw err;
            // show the one obj
            console.log(result);
        });
    },
    removeAddressById: function(id) {
        // find the Address with ID 
        Address.findByIdAndRemove(id, function(err) {
            if (err) throw err;
            // we have deleted the user Address
            console.log('Address deleted!');
        });
    }
}
exports.client = client; 