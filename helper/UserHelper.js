var config = require('../config/config');
var User = require('../models/Users');
var jwt    = require('jsonwebtoken');

var client;  

exports = module.exports = {
    createUser: function(id) {
       // create a new user
        var newUser = User({
            _id:id,
            name: 'Thirupathi Ponchetti',
            email: 'thirupathi.ponchetti@gmail.com',
            password: 'password',
            admin: true
        });

        // save the user
        newUser.save(function(err) {
            if (err) throw err;

            console.log('User created!');
        }); 
    },
    userAuthenticate: function(email,pwd) {
        // find the user
        User.findOne({
            email: email
        }, function(err, user) {
            if (err) throw err;
            if (!user) {
                res.json({ success: false, message: 'Authentication failed. User not found.' });
            } else if (user) {
                // check if password matches
                if (user.password != pwd) {
                    res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                } else {
                    // if user is found and password is right
                    // create a token
                    var token = jwt.sign(user, config.secret, {
                        expiresIn : 60 // expires in 24 hours
                    });
                    // return the information including token as JSON
                    console.log({success: true,message: 'Enjoy your token!',token: token});
                }   
            }
        });
    },
    verifyToken: function (token) {
        jwt.verify(token, config.secret, function(err, decoded) {      
            if (err) {
                console.log({ success: false, message: 'Failed to authenticate token.' });    
            } else {
                // if everything is good, save to request for use in other routes
                //req.decoded = decoded;    
                //next();
                console.log({ success: true, message: 'Success to authenticate token.' });
            }
        });
    },
    getUserById: function(userId) {
        // get a user with ID of 1
        User.findById(userId, function(err, user) {
        if (err) throw err;

        // show the one user
        console.log(user);
        });
    }
}
exports.client = client; 