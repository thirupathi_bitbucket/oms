var config = require('../config/config');
var User = require('../models/Users');

var client;  

exports = module.exports = {
    createUser: function(id) {
       console.log('--helper - create user---');
       // create a new user
        var newUser = User({
            _id:id,
            name: 'Thirupathi Ponchetti',
            username: 'thirupathi',
            password: 'password',
            admin: true
        });

        // save the user
        newUser.save(function(err) {
            if (err) throw err;

            console.log('User created!');
        }); 
    },
    getAllUsers: function() {

        // get all the users
        User.find({}, function(err, users) {
            if (err) throw err;

            // object of all the users
            console.log(users);
        });

    },
    getUserByName: function() {
        // get the user thirupathi
        User.find({ username: 'thirupathi' }, function(err, user) {
            if (err) throw err;

            // object of the user
            console.log(user);
        });
    },
    getUserById: function(userId) {
        // get a user with ID of 1
        User.findById(userId, function(err, user) {
        if (err) throw err;

        // show the one user
        console.log(user);
        });
    },
    getUserAndUpdateById: function(userId) {
        // get a user with ID of 1
        User.findById(userId, function(err, user) {
        if (err) throw err;

        // change the users location
        user.location = 'uk';

        // save the user
        user.save(function(err) {
            if (err) throw err;

            console.log('User successfully updated!');
        });

        });
    },
    findUserAndUpdate: function(oldUserName,newUserName){
        // find the user 
        // update him to newUserName
        User.findOneAndUpdate({ username: oldUserName }, { username: newUserName }, function(err, user) {
            if (err) throw err;

            // we have the updated user returned to us
            console.log(user);
        });
    },
    RemoveUserById: function(id) {
        // find the user with id 
        User.findByIdAndRemove(id, function(err) {
            if (err) throw err;

            // we have deleted the user
            console.log('User deleted!');
        });
    }
}
exports.client = client; 