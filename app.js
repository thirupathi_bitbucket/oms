// =======================
// get the packages we need ============
// =======================
var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config/config'); // get our config file
var User   = require('./models/Users'); // get our mongoose model
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var path = require('path');
var fs = require('fs');

// =======================
// configuration =========
// =======================
var routes = require('./routes/index');
var port = process.env.PORT || 3000; // used to create, sign, and verify tokens
mongoose.connect(config.database); // connect to database
app.set('superSecret', config.secret); // secret variable



app.set('env',"development");
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
var jsonParser       = bodyParser.json({limit:'150mb'});
var urlencodedParser = bodyParser.urlencoded({ extended:false,limit:'150mb',parameterLimit:50000})
app.use(jsonParser);
app.use(urlencodedParser);

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'config')));
app.use('/static',express.static(path.join(__dirname, 'public')));
app.use('/v1', routes);
app.disable('etag');

// dynamically include routes (Controller)
fs.readdirSync('./controllers').forEach(function (file) {
  if(file.substr(-3) == '.js') {
      route = require('./controllers/' + file);
      route.controller(app);
  }
});

module.exports = app;
