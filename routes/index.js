/* - - - - - - - - - - - - - - - - - - - - - - - -    
    API service for providing the search.
 - - - - - - - - - - - - - - - - - - - - - - - - */


var express = require('express');
var router = express.Router();

router.get('/test', function(req, res, next) {
  res.send('Testing......');
});

module.exports = router;
