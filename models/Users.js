// grab the things we need
var config = require('../config/config');
var mongoose = require('mongoose'),
bcrypt = require('bcrypt'),
SALT_WORK_FACTOR = 10,
// these values can be whatever you want - we're defaulting to a
// max of 5 attempts, resulting in a 2 hour lock
MAX_LOGIN_ATTEMPTS = 5,
LOCK_TIME = 2 * 60 * 60 * 1000;
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;
//connect to database
var db = mongoose.connect(config.database);
// create a schema
var UserSchema = new Schema({
  _id: String,
  name: String,
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  admin: { type: Boolean, default: false },
  userAgent:{type: Array, default:[]},
  tokens:{type: Array, default:[]},
  profile: {
    images: {type: Array, default:[]},
    name: String,
    website: {type: Array, default:[]}
  },
  loginAttempts: { type: Number, required: true, default: 0 },
  lockUntil: { type: Number },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

UserSchema.virtual('isLocked').get(function() {
    // check for a future lockUntil timestamp
    return !!(this.lockUntil && this.lockUntil > Date.now());
});

UserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);

            // set the hashed password back on our user document
            user.password = hash;
            next();
        });
    });
});

// the schema is useless so far
// we need to create a model using it
var Users = db.model('Users', UserSchema);

// make this available to our users in our Node applications
module.exports = Users;