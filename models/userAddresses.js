// grab the things we need
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;
//connect to database
var db = mongoose.connect('mongodb://127.0.0.1:27017/oms');
// create a schema
var addressSchema = new Schema({
  _id: String,
  name: String,
  address: Object,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

// the schema is useless so far
// we need to create a model using it
var UserAddresses = db.model('UserAddresses', addressSchema);

// make this available to our UserAddresses in our Node applications
module.exports = UserAddresses;