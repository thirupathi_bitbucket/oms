// grab the things we need
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;
//connect to database
var db = mongoose.connect('mongodb://127.0.0.1:27017/oms');
// create a schema
var orderSchema = new Schema({
  _id: String,
  pFlight:Object,
  pHotel:Object,
  pParking:Object,
  pTransers:Object,
  _status: Object,
  orderInfo: Object,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

// the schema is useless so far
// we need to create a model using it
var Orders = db.model('Orders', orderSchema);

// make this available to our Orders in our Node applications
module.exports = Orders;